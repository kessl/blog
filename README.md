# Static blog site with Next.js

Generates static HTML with no Javascript from Markdown-formatted files. Post metadata is stored inside the Markdown files in [frontmatter](https://jekyllrb.com/docs/front-matter/) format.

Read more & see it live at [blog.bitgate.cz](https://blog.bitgate.cz/making-of-this-blog/).

## How to use

Clone the repo & install:

```shell
$ git clone https://gitlab.com/kessl/blog
$ cd blog && yarn
```

Create a new `.md` file in `POSTS_DIR` (default `./posts`):

```md
---
title: New post
slug: new-post
tags: ['blog']
author: Daniel
created: 2021-01-01T23:00:00.000
---

# Post title

This is a brand new post.
```

Start the dev server with `yarn dev`, build with `yarn build`, export HTML with `yarn export`.

Preview on [localhost:3000](http://localhost:3000). Once done writing, commit, push to GitLab, view on GitLab pages.

### Environment variables

| Variable name        | Description                         | Default    |
| -------------------- | ----------------------------------- | ---------- |
| POSTS_DIR            | Directory with Markdown files       | posts      |
| NEXT_PUBLIC_BASE_URL | Deployment base URI used in OG tags | _no value_ |
