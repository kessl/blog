export type CodeProps = {
  className?: string
}

export const Code: React.FC<CodeProps> = ({ children, className }) => (
  <code className={className}>{children}</code>
)
