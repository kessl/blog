import cn from 'classnames'
import { Post as PostType } from 'utils/posts'
import { Link, Code } from '.'

type PostProps = {
  post: PostType
}

export const Post: React.FC<PostProps> = ({ post }) => (
  <>
    <div>
      <p className="mt-15 font-mono text-14 leading-size-14">
        written by {post.attributes.author}
        {!!post.attributes.tags.length && (
          <span>
            &nbsp;in{' '}
            {post.attributes.tags.map((tag, i) => (
              <Link to="/topics/[tag]" as={`/topics/${tag}`} key={tag}>
                <Code className={cn('text-14', i < post.attributes.tags.length - 1 && 'mr-5')}>
                  {tag}
                </Code>
              </Link>
            ))}
          </span>
        )}
        <span>&nbsp;on {new Date(post.attributes.created).toDateString()}</span>
      </p>
    </div>
    <div dangerouslySetInnerHTML={{ __html: post.html }} />
  </>
)
