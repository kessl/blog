import { useRouter } from 'next/dist/client/router'

const PIXEL_URL = 'https://t.bitgate.cz/cat.gif?u='

export const Pixel: React.FC = () => {
  const router = useRouter()
  const currentUrl = encodeURIComponent(`${process.env.NEXT_PUBLIC_BASE_URL}${router.asPath}`)

  return (
    <img src={`${PIXEL_URL}${currentUrl}`} decoding="async" loading="eager" className="invisible" />
  )
}
