import cn from 'classnames'
import NextLink from 'next/link'

/* eslint-disable react/jsx-no-target-blank -- _blank is used together with noreferrer */

export type LinkProps = {
  // internal link
  to?: string
  as?: string

  // external link
  href?: string

  openInNewTab?: boolean
  isFormSubmit?: boolean
  className?: string
}

export const Link: React.FC<LinkProps> = props => {
  const { children, to, as, href, openInNewTab, className, ...rest } = props

  if (to) {
    return (
      <NextLink href={to} as={as}>
        <a
          target={openInNewTab ? '_blank' : undefined}
          rel={openInNewTab ? 'noreferrer' : undefined}
          {...rest}
          className={cn('underline', className)}
        >
          {children}
        </a>
      </NextLink>
    )
  }

  return (
    <a
      href={href}
      target={openInNewTab ? '_blank' : undefined}
      rel={openInNewTab ? 'noreferrer' : undefined}
      {...rest}
      className={cn('underline', className)}
    >
      {children}
    </a>
  )
}
