import { Post } from 'utils/posts'
import { Link, Code } from '.'

type PostListProps = {
  posts: Post[]
  noTags?: boolean
}

export const PostList: React.FC<PostListProps> = ({ posts, noTags }) => (
  <ul className="p-0">
    {posts.map(post => (
      <li
        className="flex flex-col sm:flex-row justify-between mt-20 sm:mt-10"
        key={post.attributes.slug}
      >
        <div>
          <Link to="/[post]" as={`/${post.attributes.slug}`}>
            {post.attributes.title}
          </Link>
          {!noTags &&
            post.attributes.tags.map(tag => (
              <Code className="text-14 ml-5" key={tag}>
                {tag}
              </Code>
            ))}
        </div>
        <p className="font-mono text-14 text-foreground-lighter flex-shrink-0">
          {new Date(post.attributes.created).toDateString()}
        </p>
      </li>
    ))}
  </ul>
)
