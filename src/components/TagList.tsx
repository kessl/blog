import { TagInfo } from 'utils/posts'
import { Link, Code } from '.'

type TagListProps = {
  tags: TagInfo[]
}

export const TagList: React.FC<TagListProps> = ({ tags }) => (
  <ul className="list-none p-0 mt-15 text-16">
    {tags.map(tagInfo => (
      <li className="mt-10" key={tagInfo.tag}>
        <Link to="/topics/[tag]" as={`/topics/${tagInfo.tag}`}>
          <Code>{tagInfo.tag}</Code>
        </Link>
        <span className="font-mono">&nbsp;({tagInfo.count})</span>
      </li>
    ))}
  </ul>
)
