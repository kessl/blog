import { Post } from './posts'

function escapeHtml(unsafe: string) {
  return unsafe
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#039;')
}

export function generateFeed(posts: Post[]) {
  return `<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <title>bitgate blog</title>
  <link href="${process.env.NEXT_PUBLIC_BASE_URL}" />
  <updated>${new Date().toISOString()}</updated>
  <author>
    <name>Daniel Kessl</name>
  </author>
  <id>${process.env.NEXT_PUBLIC_BASE_URL}</id>

  ${posts
    .map(
      post => ` <entry>
    <title type="html">${escapeHtml(post.attributes.title)}</title>
    <link href="${`${process.env.NEXT_PUBLIC_BASE_URL}/${post.attributes.slug}`}" />
    <id>${`${process.env.NEXT_PUBLIC_BASE_URL}/${post.attributes.slug}`}</id>
    <updated>${new Date(post.attributes.created).toISOString()}</updated>
    ${post.attributes.tags.map(tag => `<category term="${tag}" />`).join('')}
    <content type="html">${escapeHtml(post.html)}</content>
  </entry>`
    )
    .join('')}
</feed>
  `
}
