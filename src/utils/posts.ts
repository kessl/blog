import { promises as fs } from 'fs'

type PostMetadata = {
  title: string
  slug: string
  tags: string[]
  author: string
  created: string
}

export type Post = {
  attributes: PostMetadata
  html: string
}

export async function findPosts(): Promise<Post[]> {
  if (!process.env.POSTS_DIR) {
    throw new Error('Environment variable POSTS_DIR not defined')
  }

  const directory = await fs.readdir(process.env.POSTS_DIR)
  const files = await Promise.all(
    directory.map(file => import(`../../${process.env.POSTS_DIR}/${file}`))
  )
  const posts: Post[] = files.map(module => module.default)

  return posts.sort((a, b) =>
    new Date(a.attributes.created) > new Date(b.attributes.created) ? -1 : 0
  )
}

export type TagInfo = {
  tag: string
  count: number
}

export async function findTags(): Promise<TagInfo[]> {
  const posts = await findPosts()
  return Object.entries(
    posts
      .flatMap(post => post.attributes.tags)
      .reduce((acc, tag) => ({ ...acc, [tag!]: ++acc[tag!] || 1 }), {})
  )
    .map(([tag, count]) => ({ tag, count: count as number }))
    .sort((a, b) => (a.count > b.count ? -1 : a.tag > b.tag ? 1 : 0)) // sort by post count first, then alphabetically
}
