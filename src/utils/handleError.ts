import { ErrorInfo } from 'react'

type ErrorsType = {
  error?: Error
  errorInfo?: ErrorInfo
}

export function handleError(e: ErrorsType) {
  if (e.error) {
    console.group(`[Error]: ${e.error.message}`)
    console.error(e.error)
    e.errorInfo && console.error('[ErrorInfo]:', e.errorInfo)
    console.groupEnd()
  }
}
