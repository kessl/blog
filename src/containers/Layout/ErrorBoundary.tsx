import { Component, ErrorInfo } from 'react'
import { handleError } from 'utils/handleError'

type ErrorBoundaryState = {
  error?: Error
}

export class ErrorBoundary extends Component<unknown, ErrorBoundaryState> {
  state: ErrorBoundaryState = {}

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    handleError({ error, errorInfo })
  }

  static getDerivedStateFromError(error: Error) {
    return { error }
  }

  render() {
    if (this.state.error) {
      return (
        <div className="flex flex-row justify-center items-center w-full h-300">
          There was an error. Please try reloading the page.
        </div>
      )
    }

    return this.props.children
  }
}
