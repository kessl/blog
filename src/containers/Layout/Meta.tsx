import Head from 'next/head'

type MetaProps = {
  title: string
}

export const Meta: React.FC<MetaProps> = props => (
  <Head>
    <title>{props.title}</title>
    <meta
      name="description"
      content="Chronicles of frontend development, shipping containers, homelabs, drones and other near-life
      experiences."
    />
    <meta name="viewport" content="width=device-width" />

    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=yy4nlJ2z7x" />
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=yy4nlJ2z7x" />
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=yy4nlJ2z7x" color="#5bbad5" />
    <link rel="shortcut icon" href="/favicon.ico?v=yy4nlJ2z7x" />

    <meta property="og:title" content="bitgate blog" />
    {process.env.NEXT_PUBLIC_BASE_URL && (
      <meta property="og:url" content={process.env.NEXT_PUBLIC_BASE_URL} />
    )}
    <meta property="og:type" content="article" />
    <meta
      property="og:description"
      content="Chronicles of frontend development, shipping containers, homelabs, drones and other near-life
      experiences."
    />
    {process.env.NEXT_PUBLIC_BASE_URL && (
      <meta property="og:image" content={`${process.env.NEXT_PUBLIC_BASE_URL}/DK.png`} />
    )}

    {process.env.NEXT_PUBLIC_BASE_URL && (
      <link
        rel="alternate"
        type="application/atom+xml"
        href={`${process.env.NEXT_PUBLIC_BASE_URL}/atom.xml`}
      />
    )}
  </Head>
)
