import { Link } from 'components'

export const Header: React.FC = props => (
  <ul className="max-w-1040 mx-auto p-20 font-mono flex flex-wrap justify-center text-14 list-none space-x-25 sm:space-x-45">
    <li>
      <Link to="/">/</Link>
    </li>
    <li>
      <Link to="/archive">/archive</Link>
    </li>
    <li>
      <Link to="/topics">/topics</Link>
    </li>
    <li>
      <Link to="/[post]" as="/about">
        /about
      </Link>
    </li>
  </ul>
)
