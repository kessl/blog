import { Pixel } from 'components'
import { ErrorBoundary, Header, Meta } from '.'

type MainLayoutProps = {
  title?: string
}

export const MainLayout: React.FC<MainLayoutProps> = props => (
  <>
    <Meta title={props.title!} />
    <Header />
    <div className="xl:flex xl:items-start xl:flex-row-reverse xl:justify-center mb-30">
      <main className="max-w-900 mx-auto xl:mx-0 px-20 md:px-30 pt-1 pb-20 flex-grow">
        <ErrorBoundary>{props.children}</ErrorBoundary>
      </main>
    </div>
    <Pixel />
  </>
)

MainLayout.defaultProps = {
  title: 'blog | bitgate',
}
