import { Post } from 'components'
import { MainLayout } from 'containers'
import { promises as fs } from 'fs'
import { GetStaticPaths, GetStaticProps, NextPage } from 'next'
import { generateFeed } from 'utils/generateFeed'
import { findPosts, Post as PostType } from 'utils/posts'

export const config = {
  unstable_runtimeJS: false,
}

type PostPageProps = {
  post: PostType
}

const PostPage: NextPage<PostPageProps> = ({ post }) => (
  <MainLayout title={`${post.attributes.title} | bitgate`}>
    <Post post={post} />
  </MainLayout>
)

export default PostPage

export const getStaticProps: GetStaticProps<PostPageProps> = async ctx => {
  if (!ctx.params?.post) {
    throw new Error('Missing post id in PostPage.getStaticProps')
  }

  const slug = Array.isArray(ctx.params.post) ? ctx.params.post[0] : ctx.params.post

  const posts = await findPosts()
  const post = posts.find(p => p.attributes.slug === slug)

  if (!post) {
    throw new Error(`Post '${slug}' not found in getStaticProps`)
  }

  return {
    props: { post },
  }
}

export const getStaticPaths: GetStaticPaths = async () => {
  const posts = await findPosts()

  // HACK: generates Atom feed. Ugly, should not be here, but easy and efficient
  const feed = generateFeed(posts)
  await fs.writeFile('./public/atom.xml', feed)

  return {
    paths: posts.map(post => ({ params: { post: post.attributes.slug } })),
    fallback: false,
  }
}
