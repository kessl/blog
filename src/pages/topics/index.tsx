import { GetStaticProps, NextPage } from 'next'
import { PostList, TagList } from 'components'
import { MainLayout } from 'containers'
import { findPosts, findTags, Post, TagInfo } from 'utils/posts'

export const config = {
  unstable_runtimeJS: false,
}

type TopicsPageProps = {
  tags: TagInfo[]
  untagged: Post[]
}

const TopicsPage: NextPage<TopicsPageProps> = ({ tags, untagged }) => (
  <MainLayout title="topics | bitgate">
    <p className="mt-15 font-mono text-14 leading-size-14">all posts by topic</p>
    <h1 className="mt-30">Topics</h1>
    <TagList tags={tags} />
    {untagged.length !== 0 && (
      <>
        <h3>Posts with no topic</h3>
        <PostList posts={untagged} noTags />
      </>
    )}
  </MainLayout>
)

export default TopicsPage

export const getStaticProps: GetStaticProps = async ctx => {
  const tags = await findTags()
  const posts = await findPosts()
  const untagged = posts.filter(post => post.attributes.tags.length === 0)

  return {
    props: {
      tags,
      untagged,
    },
  }
}
