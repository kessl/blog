import { GetStaticPaths, GetStaticProps, NextPage } from 'next'
import { Code, PostList } from 'components'
import { MainLayout } from 'containers'
import { findPosts, findTags, Post, TagInfo } from 'utils/posts'

export const config = {
  unstable_runtimeJS: false,
}

type TagPageProps = {
  tag: TagInfo
  posts: Post[]
}

const TagPage: NextPage<TagPageProps> = ({ tag, posts }) => (
  <MainLayout title={`posts tagged ${tag.tag} | bitgate`}>
    <h1 className="mt-30">
      Posts tagged <Code className="text-42">{tag.tag}</Code>
    </h1>
    <PostList posts={posts} noTags />
  </MainLayout>
)

export default TagPage

export const getStaticProps: GetStaticProps<TagPageProps> = async ctx => {
  if (!ctx.params?.tag) {
    throw new Error('Missing tag in TagPage.getStaticProps')
  }

  const slug = Array.isArray(ctx.params.tag) ? ctx.params.tag[0] : ctx.params.tag

  const posts = await findPosts()
  const tagPosts = posts.filter(post => post.attributes.tags.includes(slug))

  const tags = await findTags()
  const tag = tags.find(t => t.tag === slug)

  if (!tag) {
    throw new Error(`Tag '${slug}' TagPage.getStaticProps`)
  }

  return {
    props: {
      posts: tagPosts,
      tag,
    },
  }
}

export const getStaticPaths: GetStaticPaths = async () => {
  const tags = await findTags()

  return {
    paths: tags.map(tagInfo => ({ params: { tag: tagInfo.tag } })),
    fallback: false,
  }
}
