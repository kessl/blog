import { AppProps } from 'next/app'
import 'utils/global.css'

const StaticBlogApp = ({ Component, pageProps }: AppProps) => <Component {...pageProps} />

export default StaticBlogApp
