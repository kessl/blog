import { NextPage } from 'next'
import { Link } from 'components'
import { MainLayout } from 'containers'

export const config = {
  unstable_runtimeJS: false,
}

const NotFoundPage: NextPage = () => (
  <MainLayout>
    <div className="text-center my-90">
      <h2>404</h2>
      <h1 className="my-15">not found</h1>
      <p className="text-14">
        return to the <Link to="/">home page</Link>&nbsp;&middot;&nbsp;see{' '}
        <Link to="/archive">all posts</Link>
      </p>
    </div>
  </MainLayout>
)

export default NotFoundPage
