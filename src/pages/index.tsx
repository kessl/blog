import { GetStaticProps, NextPage } from 'next'
import { Link, Post } from 'components'
import { MainLayout } from 'containers'
import { findPosts, Post as PostType } from 'utils/posts'

export const config = {
  unstable_runtimeJS: false,
}

type LandingPageProps = {
  post: PostType
}

const LandingPage: NextPage<LandingPageProps> = ({ post }) => (
  <MainLayout>
    <Post post={post} />
    <div className="flex justify-end mt-15">
      <Link to="/archive" className="font-mono text-14">
        more posts &raquo;
      </Link>
    </div>
  </MainLayout>
)

export default LandingPage

export const getStaticProps: GetStaticProps<LandingPageProps> = async ctx => {
  const posts = await findPosts()
  const latestPost = posts[0]

  return {
    props: {
      post: latestPost,
    },
  }
}
