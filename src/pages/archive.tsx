import { GetStaticProps, NextPage } from 'next'
import { PostList } from 'components'
import { MainLayout } from 'containers'
import { findPosts, Post } from 'utils/posts'

export const config = {
  unstable_runtimeJS: false,
}

type ArchivePageProps = {
  posts: Post[]
}

const ArchivePage: NextPage<ArchivePageProps> = ({ posts }) => (
  <MainLayout title="archive | bitgate">
    <p className="mt-15 font-mono text-14 leading-size-14">
      all posts since the beginning of time here
    </p>
    <h1 className="mt-30">Post archive</h1>
    <PostList posts={posts} />
  </MainLayout>
)

export default ArchivePage

export const getStaticProps: GetStaticProps = async ctx => {
  const posts = await findPosts()

  return {
    props: { posts },
  }
}
