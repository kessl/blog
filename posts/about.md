---
title: About
slug: about
tags: []
author: Daniel
created: 2021-06-27T23:00:00.000
---

# The web is in my DNA

I'm a software developer.
I make machines do things.

Write me an [email](mailto:dan@kessl.net) or let's connect on [LinkedIn](https://linkedin.com/in/kessl/) if you'd like to get in touch.

### Better Stack

Currently I work with Better Stack on developing next-generation observability tools that enable developers to create more reliable software faster.

`Ruby` `Rails` `Javascript` `REST` `Tailwind` `Rust` `Unix`

### BankID

I helped create BankID while working with [Applifting](https://applifting.cz), a digital products agency and [teal organization](https://en.wikipedia.org/wiki/Teal_organisation).
BankID enables users to seamlessly log in to third party services through their bank.

I developed the [BankID developer portal](https://developer.bankid.cz), end user bank selection screen, self-care portal and backoffice interface, and provisioned a CMS and an image resizing tool.

`Typescript` `React` `Next.js` `GraphQL` `REST` `Tailwind` `Docker` `Kubernetes` `GitLab CI` `OAuth2` `Node.js`

### DX Scanner

I made an interactive metrics dashboard and a landing page for [DX Scanner](https://dxscanner.io), an open source tool by [DX Heroes](https://dxheroes.io) which helps teams improve their developer experience.

`Typescript` `React` `Next.js` `GraphQL` `Tailwind` `Docker` `Kubernetes` `GitLab CI`
