const withImages = require('next-optimized-images')
const markdownIt = require('markdown-it')
const markdownItPrism = require('markdown-it-prism')

module.exports = phase =>
  withImages({
    // TODO: install image optimization packages
    // https://github.com/cyrilwanner/next-optimized-images#optimization-packages
    optimizeImages: false,

    // needed for index pages due to GitLab directory redirects
    trailingSlash: true,

    webpack: config => {
      // load markdown files as HTML
      config.module.rules.push({
        test: /\.md$/,
        loader: 'frontmatter-markdown-loader',
        options: {
          markdownIt: markdownIt({ html: true }).use(markdownItPrism),
        },
      })

      return config
    },
  })
