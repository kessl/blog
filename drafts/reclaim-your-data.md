# Reclaim your data

**TL;DR** You pay for "free" services with your data. I suggest some options to control what data you give out.

Corporations collect data about you, your social interactions, your online behavior
Your data is used to influence your decisions

But what if I actually like being shown relevant ads? At least when the alternative is being shown totally random ads. The problem is, advertisers optimize ad relevance not based on which products you'd benefit from the most, but based on which ads you're most likely to click on and most likely to buy.

What's better, there is another alternative, and that's blocking ads altogether. Regardless of whether you find being shown relevant ads acceptable or not, fundamentally ad companies are taking data that belongs to you, without asking or even without you knowing, and using it to further their goals.

Services provided by these companies provide undeniable value and are a firm part of the life we live today. I use some of them every day in my personal life and at work. What I find off-putting is the way these services are presented to the end user as free, when in fact you pay with your data. This deception runs deep at the heart of ad companies - just listen to Zuckerberg's [evasive answers](https://www.theguardian.com/technology/2018/apr/11/mark-zuckerbergs-testimony-to-congress-the-key-moments) to questions in front of the US congress or Google's [employee NDAs](https://www.insider.com/google-culture-secrecy-employee-lawsuit-spying-program-novel-2016-12).

This fundamental deception is the reason I have no qualms about minimizing the amount of personal data I give out to these services. I provide made-up info, block tracking and ads. They claim the service is free, and there is not option of paying for the service with anything other than your data. There is no paid Facebook tier where you opt out of having your data extracted and sold to advertisers and political marketers. I'm making sure it stays free.

Let's look at a few data extraction vectors in everyday life, why it might be a good idea to consider avoiding them beyond the philosophical reasons I outlined above, and how it can be done. Given how deeply incorporated some of these services are into our lives, I'll suggest a practical option in addition to just giving the service up.

## Cloud services

E-mail, calendar, documents, file storage

**Practical option** Back up your data to some other cloud service or offline.

**Total solution** Self-host a cloud solution such as Nextcloud, encrypt all your data.

## Web browsers

Conflict of interest in a browser made by an advertising company.

**Practical option** Use [Firefox](https://www.mozilla.org/en-US/firefox/new/) with [uBlock Origin](https://addons.mozilla.org/cs/firefox/addon/ublock-origin/). Set up [Multi-account Containers](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/) to lock Facebook, Google and Microsoft services inside their container.

**Total solution** Disable Javascript.

## Search

DuckDuckGo

## Your phone

Don't use a phone/OS made by an advertising company. Use encrypted messaging apps such as Signal and Telegram. Do not install any native tracking apps - no Facebook, Messenger, Instagram, Youtube, G-Suite apps. Give apps only the permissions you will use.

## Marketing consent

Don't give marketing consent where not needed.

https://beepb00p.xyz/myinfra.html
