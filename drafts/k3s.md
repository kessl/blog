# Rise of the machines

- hardware

  - Raspberry Pi 3B+
  - Odroid HC1
  - SD cards

- prepare devices

  - Raspbian
    - touch ssh in boot partition
    - ssh pi@raspberrypi.local
    - sudo raspi-config
      - GPU mem split 16 MB
      - hostname
    - enable container features
      - append to /boot/cmdline.txt:
        cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory
    - reboot
  - DietPi
    - find out IP - nmap 192.168.0.0/24 -p22
    - run setup
      - hostname
  - set static IPs
  - copy SSH key
    - ls -l ~/.ssh/id_rsa.pub
    - ssh-keygen
    - ssh-copy-id pi@raspberry.local

- install k3s

# Living on the edge

- MetalLb
- Traefik
- cert-manager
- DNS

# Persistent storage

# Making it do things

- nginx & goaccess analytics
- NextCloud
- B2 backup
- Plex
- monitoring
